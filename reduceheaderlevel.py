# Script to reduce the level of all section headers in MarkDown by one.
# Necessary for sections that are separate files,
# as JupyterBook automatically adds one level.

def HeaderLevelReduce(matchobject):
    return matchobject.group(0)[1:]

def ReduceHeaderLevel(filename):
    # Main function. Open the file in readonly mode.
    # Cleanup LaTeX through successive substitutions.
    with open(filename+".md", 'r') as file:
        source = file.read()
        result = re.sub(r"#+", HeaderLevelReduce, source, 0, re.MULTILINE)

    if result:
        print("Reduction successful!")
        with open(filename+".md", 'w') as file:
            # Write the converted file as markdown.
            file.write(result)
            file.truncate()

if __name__ == "__main__":
    import sys
    import re
    import os.path
    # Read filename (first argument) and name of file with (image) sources (optional second argument). Note that the zeroth argument is the script name.
    filename = sys.argv[1]
    #basepath = "/Users/timonidema/Documents/Teaching/mathappendix/"
    basepath = "./"
    if not os.path.isfile(basepath+filename+".md"):
        print('File does not exist.')
    else:
        ReduceHeaderLevel(basepath+filename)
