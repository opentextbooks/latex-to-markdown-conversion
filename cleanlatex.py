# Script for cleaning up LaTeX, using the regular expressions library (see https://docs.python.org/3/library/re.html).
# Output of the LaTeX should not change, but will be useful as precursor for LaTeX to MarkDown.
# Created by Timon Idema on 17-08-2023.

# 1) Remove all comments, except the ones starting with % Problemtitle and % Source.
# 2) Replace all \\ in text with \newline
# 3) Remove all superfluous whitespace.

def LineCommentReplace(matchobject):
    # Remove a single-line comment, unless it starts with a keyword.
    if (matchobject.group(1) != None): return matchobject.group(0)
    else: return ""
    
def CommentRemove(matchobject):
    # Remove comment (but not the symbol before %).
    return matchobject.group(1)

def RemoveImproperLineBreaks(matchobject):
    # Remove an improper line break inside a sentence.
    return " " + matchobject.group(2)

def CleanLatex(latexfile):
    result = latexfile

    # Remove comments.
    # Zeroth, remove lines with just a %.
    result = re.sub(r"%\n", "\n", result, 0, re.MULTILINE)

    # First, comments that start at the start of a line.
    # Remove unless they start with a keyword (Problemtitle or Source).
    regex = r"^%\s?(Problemtitle|Source)?(.*)\s"
    result = re.sub(regex, LineCommentReplace, result, 0, re.MULTILINE | re.IGNORECASE)

    # Second, remove comments that start somewhere in a line.
    # Note we need to exclude linestarts and \% (for percent sign in eq.).
    # Just remove.
    regex = r"([^\n\\])(%.*)\s"
    result = re.sub(regex, CommentRemove, result, 0, re.MULTILINE)

    # Replace any 4+ multiples of \; with \quad.
    regex = r"(\\;){4,}"
    result = re.sub(regex, r"\\quad", result, 0, re.MULTILINE)

    # Strip any spaces before endline.
    regex = r" +\n"
    result = re.sub(regex, r"\n", result, 0, re.MULTILINE)

    # Strip any spaces at the start of a line.
    # Could extend to tabs with "^[ \t]+", but leave those for now.
    regex = r"^ +"
    result = re.sub(regex, "", result, 0, re.MULTILINE)

    # Split lines after a "\\"
    regex = r"\\\\[^\n]"
    result = re.sub(regex, r"\\\\\n", result, 0, re.MULTILINE)

    # Replace any standalone "\\" with "\newline".
    regex = r"^\\\\\n"
    result = re.sub(regex, r"\\newline\n", result, 0, re.MULTILINE)

    # Replace any end of sentence with "\\" with "\newline".
    regex = r"\.[ ]+\\\\\n"
    result = re.sub(regex, r". \\newline\n", result, 0, re.MULTILINE)

    # For Aurele's book: {\bf Remark.} \\
    regex = r"\{\\bf Remark.\}[ ]*\\\\"
    result = re.sub(regex, r"{\\bf Remark.} \\newline", result, 0, re.MULTILINE)
    regex = r"\{\\bf Remarks.\}[ ]*\\\\"
    result = re.sub(regex, r"{\\bf Remarks.} \\newline", result, 0, re.MULTILINE)

    # Remove any endlines inside a sentence.
    # \b is word-boundary.
    # With the [a-z], we only select cases where the new line starts with a lowercase letter (could be extended, or even left out and have all word boundaries.)
    regex = r"\b( )*\n\b([a-z])"
    result = re.sub(regex, RemoveImproperLineBreaks, result, 0, re.MULTILINE)

    """
    # Remove any enlines inside a sentence (more general than above, any new actual word on next line).
    regex = r"\b( )*\n\b"
    result = re.sub(regex, " ", result, 0, re.MULTILINE)
    """

    # Replace three or more successive endlines with just two.
    regex = r"\n{3,}"
    result = re.sub(regex, "\n\n", result, 0, re.MULTILINE)

    # Replace two or more successive spaces with just one.
    regex = r" {2,}"
    result = re.sub(regex, " ", result, 0, re.MULTILINE)

    # Remove any spaces before a comma or a period.
    result = re.sub(r" \,", ",", result, 0, re.MULTILINE)
    result = re.sub(r" \.", ".", result, 0, re.MULTILINE)

    return result

def CleanLatexFile(filename):
    # Main function. Open the file in readonly mode.
    # Cleanup LaTeX through successive substitutions.
    with open(filename+".tex", 'r') as file:
        latexsource = file.read()
        result = CleanLatex(latexsource)

    if result:
        print("Cleanup successful!")
        with open(filename+"Clean.tex", 'w') as file:
            # Write the converted file as markdown.
            file.write(result)
            file.truncate()

# Main function - only to be executed if file is called as script, with an argument that is the name of the LaTeX file to be converted.
if __name__ == "__main__":
    import sys
    import re
    filename = sys.argv[1]
    basepath = "./"
    CleanLatexFile(basepath+filename)