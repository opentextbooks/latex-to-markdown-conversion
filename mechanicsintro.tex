\chapter{Introduction to classical mechanics}
\label{ch:mechanicsintroduction}
Classical mechanics is the study of the motion of bodies under the action of physical forces. A \emph{force} is any influence that can cause an object to change its velocity. The object can be anything from an elementary particle to a galaxy. Of course anything larger than an elementary particle is ultimately a composite of elementary particles, but fortunately we usually don't have to consider all those, and can \emph{coarse-grain} to the scale of the objects at hand. As is true for any physical model, classical mechanics is an approximation and has its limits - it breaks down at very small scales, high speeds and large gravitational fields - but within its range of applicability (which includes pretty much every single phenomenon in everyday life) it is extremely useful.

Classical mechanics is based on a small number of \emph{physical laws}, which are mathematical formulations of a physical observation. Some laws can be derived from others, but you cannot derive all of them from scratch. Some laws are \emph{axioms}, and we'll assume they are valid. The laws we'll encounter can be divided up in three classes: Newton's laws of motion, conservation laws and force laws. As we'll see, the three conservation laws of classical mechanics (of energy, momentum and angular momentum) can be derived from Newton's second and third laws of motion, as can Newton's first law. The force laws give us the force exerted by a certain physical system - a compressed spring (Hooke's law) or two charged particles (Coulomb's law) for example. These also feed back into Newton's laws of motion, although they cannot be derived from these and are axioms by themselves.

In addition to the physical laws, there is a large number of \emph{definitions} - which should not be confused with the laws. Definitions are merely convenient choices. A good example is the definition of the number $\pi$: half the ratio of the circumference to the radius of a circle. As you have no doubt noticed, it is very convenient that this number has gotten its own symbol that is universally recognized, as it pops up pretty much everywhere. However, there is no axiom here, as we are simply taking a ratio and giving it a name.

\section{Dimensions and units}
In physics in general, we are interested in relating different \index{physical quantities}\emph{physical quantities} to one another - we want to answer questions like `how much work do I need to do to get this box up to the third floor'? In order to be able to give an answer, we need certain measurable quantities as input - in the present case, the mass of the box and the height of a floor. Then, using our laws of physics, we will be able to produce another measurable quantity as our answer - here the amount of work needed. Of course, you could check this answer, and thus validate our physical model of reality, by measuring the quantity in question.

\begin{table}[t]
\centering
\begin{tabular}{|l|c|l|c|l|}
\hline
\textbf{quantity} & \textbf{symbol} & \textbf{unit} & \textbf{symbol} & \textbf{based on}\\
\hline
length & $L$ & meter & $\mathrm{m}$ & speed of light\\
time & $T$ & second & $\mathrm{s}$ & caesium atom oscillation\\
mass & $M$ & kilogram & $\mathrm{kg}$ & Planck's constant\footnotemark\\
current & $I$ & Amp\`{e}re & $\mathrm{A}$ & electron charge\\
temperature & $T$ & Kelvin & $\mathrm{K}$ & Boltzmann's constant\\
luminosity & $J$ & candela & $\mathrm{cd}$ & monochromatic radiation\\
particle count & $N$ & mole & $\mathrm{mol}$ & Avogadro's constant\\
\hline
\end{tabular}
\caption{Overview of the SI quantities and units, and the physical constants they are (or are proposed to be) based on.}
\label{tab:SIunits}
\end{table}
\footnotetext{At the time of writing, the unit of mass is still determined using a prototype in Paris, however, a redefined unit based on the value of Planck's constant is expected to be adopted on May 20, 2019.}

Measurable (or `physical', or `observational') quantities aren't just numbers - the fact that they correspond to something physical matters, and 10 seconds is something very different from 10 meters, or 10 kilograms. The term we use to express this is, rather unfortunately, to say that physical quantities have a \index{dimensions|textbf}\emph{dimension} - not to be confused with length, height and width. Anything that has a dimension can be measured, and to do so we use \index{units|textbf}\emph{units} - though there may be different units in which we measure the same quantity, such as centimeters and inches for length. When measuring the same quantity in different units, you can always convert between them - there are 2.54 centimeters in an inch - but it's meaningless to try to convert centimeters into seconds, because length and time are different quantities - they have different dimensions.

We will encounter only three different basic quantities, which have the dimensions of length ($L$), time ($T$), and mass ($M$). Thanks to the Napoleonic conquest of Europe in the early 1800s, we have a basic unit for each of these: meters (m) for length, seconds (s) for time, and kilograms (kg) for mass. Although we won't encounter them here, the standard system of units (called the \index{Syst\`eme International}Syst\`eme International, or SI) has four more of these basic pairs: (electric) current~$I$, measured in Amp\`eres (A), temperature~$T$, measured in Kelvin (K), luminosity~$J$, measured in candelas (cd), and `amount of stuff', measured in moles (mol), see table~\ref{tab:SIunits}. Unfortunately, although this system is commonly used in (continental) Europe and in many other parts of the world, it is not everywhere, notably in the US, where people persist in using such things as inches and pounds, so you'll often have to convert between units.

From the seven basic quantities in the SI, all others can be derived. For example, speed is defined as the distance traveled (length) divided by the time it took, so speed has the dimension of $L/T$ and is measured in units of $\mathrm{m}/\mathrm{s}$. Note that in order to be able to compare two quantities, they must have the same dimension. This simple observation has an important consequence: in any physics equation, the dimensions on both sides of the equality sign always have to be the same. There's no bargaining on this point: equating two quantities with different dimensions does not make any kind of sense, so if you find that that's what you're doing at any point, backtrack and find where things went wrong.

\section{Dimensional analysis}
\label{sec:dimanalysis}
Although you will of course need a complete physical model (represented as a set of mathematical equations) to fully describe a physical system, you can get surprisingly far with a simple method that requires no detailed knowledge at all. This method is known as \index{dimensional analysis|textbf}\emph{dimensional analysis}, and based on the observation in the previous section that the two sides of any physical equation have to have the same dimension. You can use this principle to qualitatively understand a system, and make predictions on how it will respond quantitatively if you change some parameter. To understand how dimensional analysis works, an example is probably the most effective - we'll take one that is ubiquitous in classical mechanics: a mass oscillating on a spring (known as the \index{harmonic oscillator|textbf}\emph{harmonic oscillator}), see figure~\ref{fig:harmonicoscillator}.

\begin{figure}
\begin{center}
\includegraphics[scale=1]{mechanics/figures/PDF/harmonicoscillator.pdf}
\end{center}
\caption{A harmonic oscillator: a mass~$m$ suspended on a spring with spring constant~$k$, oscillating with a frequency~$\omega$.}
\label{fig:harmonicoscillator}
\end{figure}

%\begin{wrapfigure}[12]{r}{0.25 \columnwidth}
%\includegraphics[scale=1]{harmonicoscillator.pdf}
%\caption{A harmonic oscillator: a mass~$m$ suspended on a spring with %spring constant~$k$, oscillating with a frequency~$\omega$.}
%\label{fig:harmonicoscillator}
%\end{wrapfigure}

\subsection{Worked example: Dimensional analysis of the harmonic oscillator}
Consider the harmonic oscillator consisting of a mass of magnitude~$m$, suspended on a spring with spring constant~$k$. If you pull down the mass a bit and release, it will oscillate with a frequency~$\omega$. Can we predict how this frequency will change if we double the mass?

There are two ways to answer this question. One is to consider all the forces acting on the mass, then use Newton's second law to derive a differential equation (known as the equation of motion) for the mass, solve it, and from the solution determine what happens if we change the mass. The second is to consider the dimensions of the quantities involved. We have a mass, which has dimension of mass~($M$), as it is one of our basic quantities. We have a spring with spring constant~$k$, which has dimensions of force per unit length, or mass per unit time squared:
\begin{equation}
\label{springconstantdimension}
[k] = F/L = M L T^{-2} / L = M / T^2.
\end{equation}
Note the notation $[k]$ for the dimension of $k$. For the frequency, we have $[\omega] = 1/T$. Now we know that the frequency is a function of the spring constant and the mass, and that both sides of that equation must have the same sign. Since there is no mass in the dimension of the frequency, but it exists in the dimension of both the spring constant and the mass, we know that $\omega$ must depend on the ratio of $k$ and $m$: $\omega \sim k/m$. Now $[k/m] = 1/T^2$, and from $[\omega] = 1/T$, we conclude that we must have
\begin{equation}
\label{hofreqscaling}
\omega \sim \sqrt{k/m}.
\end{equation}
Equation~(\ref{hofreqscaling}) allows us to answer our question immediately: if we double the mass, the frequency will decrease by a factor~$\sqrt{2}$.

Note that in equation~(\ref{hofreqscaling}) I did not write an equals sign, but a `scales as' sign ($\sim$, sometimes also written as $\propto$). That is because dimensional analysis will not tell us about any numerical factor that may appear in the expression, as those numerical factors have no unit (or, more correctly, have no dimension - they are \emph{dimensionless}).

You may object that there might be another factor at play: shouldn't gravity matter? The answer is no, as we can also quickly see from dimensional analysis. The force of gravity is given by $mg$, introducing another parameter $g$ (the gravitational acceleration) with dimension $[g] = L / T^2$. Now if the frequency were to depend on~$g$, there has to be another factor to cancel the dependence on the length, as the frequency itself is length-independent. Neither $m$ nor $k$ has a length-dependence in its dimension, and so they \emph{cannot} `kill' the $L$ in the dimension of~$g$; the frequency therefore also cannot depend on~$g$ - which we have now figured out without invoking any (differential) equations!

Above, I've sketched how you can use dimensional analysis to arrive at a physical scaling relation through inspection: we've combined the various factors to arrive at the right dimension. Such combinations are not always that easy to see, and in any case, you may wonder if you've correctly spotted them all. Fortunately, there is a more robust method, that we can also use to once again show that the frequency is independent of the gravitational acceleration. Suppose that in general $\omega$ could depend on $k$, $m$ and $g$. The functional dependence can then be written as\footnote{The actual function may of course contain multiple terms which are summed, but all those must have the same dimension. Operators like sines and exponentials must be dimensionless, as there are no dimensions of the form $\sin(M)$ or $e^L$. The only allowable dimensional dependencies are thus power laws.}
\begin{equation}
\label{dimanalysisgeneral}
[\omega] = [k^\alpha m^\beta g^\gamma] = (M/T^2)^\alpha M^\beta (L/T^2)^\gamma = M^{\alpha+\beta} T^{-2(\alpha + \gamma)} L^\gamma,
\end{equation}
which leads to three equations for the exponents:
\begin{align*}
\alpha + \beta &= 0, \\
-2(\alpha + \gamma) &= -1, \\
\gamma &= 0,
\end{align*}
which you can easily solve to find $\alpha = 1/2$, $\beta = -1/2$, $\gamma = 0$, which gives us equation~(\ref{hofreqscaling}). This method\footnote{The method is sometimes referred to as the Rayleigh algorithm, after John William Strutt, Lord Rayleigh (1842-1919), who applied it, among other things, to light scattering in the air. The result of Rayleigh's analysis can be used to explain why the sky is blue.} will allow you to get dimensional relations in surprisingly many different cases, and is used by most physicist as a first line of attack when they first encounter an unknown system.

\newpage
\section{Problems}
\begin{enumerate}[\thechapter .1]
\item \input{mechanics/problems/harmonicoscillatordimanalysis.tex}
\item \input{mechanics/problems/planckscales.tex}
\item \label{prob:Reynoldsnumbers} \input{mechanics/problems/Reynoldsnumbers.tex}
\item \label{prob:escapevelocitydimensional} \input{mechanics/problems/escapevelocity.tex}
\end{enumerate}
