import numpy as np
import matplotlib.pyplot as plt
from myst_nb import glue

def R10(r):
    # Returns R_{10}(r), with r in units of the Bohr radius.
    return 2 * np.exp(-r)

def R20(r):
    # Returns R_{20}(r), with r in units of the Bohr radius.
    return (1 / np.sqrt(2)) * (1 - r/2) * np.exp(-r/2)

def R21(r):
    # Returns R_{21}(r), with r in units of the Bohr radius.
    return (1/np.sqrt(6)) * (r/2) * np.exp(-r/2)

def R30(r):
    # Returns R_{30}(r), with r in units of the Bohr radius.
    return (2 / np.sqrt(27)) * (1 - (2*r/3) + (2/3) * pow(r/3, 2)) * np.exp(-r/3)

def R31(r):
    # Returns R_{31}(r), with r in units of the Bohr radius.
    return (8/np.sqrt(486)) * (1- r/6) * (r/3) * np.exp(-r/3)

def R32(r):
    # Returns R_{32}(r), with r in units of the Bohr radius.
    return (4/np.sqrt(2430)) * pow(r/3,2) * np.exp(-r/3)

r = np.linspace(0, 10, 1000)

fig, ax = plt.subplots(figsize=(6,4))

line10 = ax.plot(r, R10(r), label='$R_{10}(r)$')
line20 = ax.plot(r, R20(r), label='$R_{20}(r)$')
line21 = ax.plot(r, R21(r), label='$R_{21}(r)$')

line30 = ax.plot(r, R30(r), label='$R_{30}(r)$')
line31 = ax.plot(r, R31(r), label='$R_{31}(r)$')
line32 = ax.plot(r, R32(r), label='$R_{32}(r)$')

ax.axhline(y = 0, color = 'k', linestyle = ':')

ax.set_xlim(0,10)
ax.set_ylim(-0.2,1.0)
ax.set_xlabel('$r/a_0$')
ax.set_ylabel('$R_{nl}$')
ax.legend()